import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class ProgramaJaime {

	public static void main(String[] args) throws FileNotFoundException {
		Controlador control = new Controlador();
		ControladorDev controldev = new ControladorDev();
		
		System.out.println("Desea acceder a los datos de los desarrolladores(dev) o de los videojuegos(vd)");
		String opcion = control.sc.next();
		while (!opcion.equals("vd") && !opcion.equals("dev")) {
			System.out.println("opcion erronea, escoja una de las disponibles");
			opcion = control.sc.next();
		}

		if (opcion.equals("vd")) {
			control.Seleccion();
		}
		if (opcion.equals("dev")) {
			controldev.Seleccion();
		}		
	
	}

}
