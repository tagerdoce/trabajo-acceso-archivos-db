import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Controlador {

		Modelo asc = new Modelo();
		Scanner sc = new Scanner(System.in);
		
		
		public void Seleccion(){
			Controlador coza = new Controlador();
			coza.Array();
			
			System.out.println("Has accedido a vd");
			System.out.println("Buenas usuario, escriba una de las siguientes opciones");
			System.out.println("	View - visualiza todos los datos del documento");
			System.out.println("	Add - a�ade nuevos datos al archivo");
			System.out.println("	DB - visualiza los datos de la base de datos");
			System.out.println("	AddDB - a�adir nuevos datos a la base de datos");
			System.out.println("	TraerDB - a�ades datos al documento local desde la base da datos");
			System.out.println("	SubirDB - a�ades los datos locales a la base de datos");
			System.out.println("	BorrarDB - eliminas todos los datos de la base de datos");
			System.out.println("	Borrar - eliminas todos los datos del fichero");
			System.out.println("	Exit - Terminara el programa");
			asc.opcion = sc.next();
			
			while (!asc.opcion.equals("View") && !asc.opcion.equals("Add") && !asc.opcion.equals("Exit") && !asc.opcion.equals("DB") && !asc.opcion.equals("AddDB") && !asc.opcion.equals("TraerDB") && !asc.opcion.equals("SubirDB") && !asc.opcion.equals("BorrarDB")&& !asc.opcion.equals("Borrar") &&  !asc.opcion.equals("Exit")) {
				System.out.println("Opcion erronea, porfavor elija una de las disponibles");
				asc.opcion = "";
				asc.opcion = sc.next();
			}
			
			
			if (asc.opcion.equals("View")) {
				coza.View();
			}
			if (asc.opcion.equals("Add")) {
				coza.Add();
			}
			if (asc.opcion.equals("DB")) {
				coza.Consulta("SELECT * FROM datos", 0);
			}
			if (asc.opcion.equals("TraerDB")) {
				coza.Traer("SELECT * FROM datos", 0);
			}
			if (asc.opcion.equals("AddDB")) {
				
				String name = "";
				String desc = "";
				String dev = "";
				String genere = "";
				
				System.out.println("Vamos a a�adir datos a la base de datos");
				
				sc.nextLine();
				
				System.out.println("Titulo: ");
				name = sc.nextLine();
				System.out.println("Descripcion: ");
				desc = sc.nextLine();
				System.out.println("Desarrolladora: ");
				dev = sc.nextLine();
				System.out.println("Genero: ");
				genere = sc.nextLine();
				
				System.out.println("Hecho!");
				
				coza.insertaDatos(name, desc, dev, genere);
			}

			if (asc.opcion.equals("SubirDB")) {
				coza.SubirDB();
			}
			if (asc.opcion.equals("BorrarDB")) {
				coza.BorrarDB();
			}
			if (asc.opcion.equals("Borrar")) {
				coza.Borrar();
			}
			
			if (asc.opcion.equals("Exit")) {
				coza.Exit();
			}
			
		}
		
		
		
		
	
	
	public void Array() {
		asc.Titulos.add("Titulo: ");
		asc.Titulos.add("Descripcion: ");
		asc.Titulos.add("Desarrolladora: ");
		asc.Titulos.add("Genero: ");
	}
	

	public void View() {
		Controlador deview = new Controlador();
		try {

			FileReader fileReader = new FileReader(asc.filevd);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
				stringBuffer.append("\n");
			}
			fileReader.close();
			System.out.println(stringBuffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		deview.Seleccion();

	}

	public void Add() {
		Controlador deadd = new Controlador();
		try {
			PrintWriter out = new PrintWriter(new FileWriter(asc.filevd, true));
			out.write("\n");
			for (int i = 0; i < asc.Titulos.size(); i++) {
				System.out.println(asc.Titulos.get(i));
				out.write(asc.Titulos.get(i) + sc.nextLine());
				out.write("\n");
			}
			
			out.close();
		} catch (IOException e) {
			System.out.println("COULD NOT LOG!!");
		}

		System.out.println("Hecho!");

		deadd.Seleccion();
	}
	public void Borrar() {
		Controlador deborrar = new Controlador();
		
		String string= "";
		
		System.out.println("�Esta usted seguro que desea eliminar los datos del fichero? Y/N");
		
		string = sc.next();
		
		if (string.equals("Y")) {
			asc.filevd.delete();
			try {
				asc.filevd.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} if (string.equals("N")) {
			System.out.println("No se ha eliminado ningun dato");
			deborrar.Seleccion();
		}

		System.out.println("Hecho!");

		deborrar.Seleccion();
	}
	
		
	
	public void Consulta(String query, int columna) {
		Controlador deconsulta = new Controlador();
		try {
			
			Statement stmt = asc.getConexion().createStatement();
			ResultSet rset = stmt.executeQuery(query);
			while (rset.next()) {
				System.out.println("Titulo: " + rset.getString(2));
				System.out.println("Descripcion: " + rset.getString(3));
				System.out.println("Desarrolladora: " + rset.getString(4));
				System.out.println("Genero: " + rset.getString(5));
				System.out.println("");
			}
			rset.close();
			stmt.close();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		
		deconsulta.Seleccion();
	}
	
	public int insertaDatos (String Titulo, String Descripcion, String Dev, String Genero){
		Controlador deinsertar = new Controlador();
		Statement stmt;
		int r = 0;
		try {
			stmt = asc.getConexion().createStatement();
			String catSQL = null;
			catSQL = "Insert into datos (Titulo,Descripcion,Desarrolladora,Genero) values ('" + Titulo + "', '" + Descripcion + "', '" + Dev + "','"+ Genero +"')";
			r = stmt.executeUpdate(catSQL);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		deinsertar.Seleccion();
		return r;
	}
	
	public void BorrarDB() {
		Controlador deview = new Controlador();
		Statement stmt;
		try {
			stmt = asc.getConexion().createStatement();
			String catSQL = null;
			int r = 0;
			String string= "";
			
			System.out.println("�Esta usted seguro que quiere eliminar toda la base de datos? Y/N");
			
			string = sc.next();
			
			if (string.equals("Y")) {
				catSQL = "DELETE FROM datos";
				r = stmt.executeUpdate(catSQL);
			} if (string.equals("N")) {
				System.out.println("No se ha eliminado ningun dato");
				deview.Seleccion();
			}
					
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Hecho!");
		deview.Seleccion();
		
	}
	
	
	
	
	
	public void SubirDB() {
		Controlador deview = new Controlador();
		Statement stmt;
		try {
			stmt = asc.getConexion().createStatement();
			String catSQL = null;
			FileReader fileReader = new FileReader(asc.filevd);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			int i = 0;
			int r = 0;
			while ((line = bufferedReader.readLine()) != null) {
				
				if (!line.equals("")) {
				String[] parts = line.split(": ");
				String part = parts[1];
				
				
				asc.Titulos2.add(part);
				i++;
				}
				if (i > 3) {
					catSQL = "Insert into datos(Titulo,Descripcion,Desarrolladora,Genero) values ('" + asc.Titulos2.get(0) + "', '" + asc.Titulos2.get(1) + "', '" + asc.Titulos2.get(2) + "','"+ asc.Titulos2.get(3) +"')";
					r = stmt.executeUpdate(catSQL);
					i = 0;
					asc.Titulos2.clear();
				}
			}
			fileReader.close();
			System.out.println(stringBuffer.toString());
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Hecho!");
		deview.Seleccion();
		
	}
	
	
	
	public void Traer(String query, int columna) {
		
		
		Controlador detraer = new Controlador();
		try {
			Statement stmt = asc.getConexion().createStatement();
			ResultSet rset = stmt.executeQuery(query);
			PrintWriter out = new PrintWriter(new FileWriter(asc.filevd, true));
			while (rset.next()) {
				out.write("\n");
				out.write("Titulo: " + rset.getString(2));
				out.write("\n");
				out.write("Descripcion: " + rset.getString(3));
				out.write("\n");
				out.write("Desarrolladora: " + rset.getString(4));
				out.write("\n");
				out.write("Genero: " + rset.getString(5));
				out.write("\n");
			}
			rset.close();
			stmt.close();
			out.close();
		} catch (SQLException | IOException s) {
			s.printStackTrace();
		}
		System.out.println("Hecho!");
		detraer.Seleccion();
	}
	
	
	public void Exit() {}
	
	
	
}
